import 'package:flutter/material.dart';

class SalesWidget extends StatelessWidget {
 final Color color;
 final String title = 'Sales';

 SalesWidget(this.color);

 @override
 Widget build(BuildContext context) {
   return Scaffold(appBar: AppBar(
        title: Text(title),
      ),
      body: Container(
        color: color,
      ),
    );
  //  return Container(
  //    color: color,
  //  );
 }
}
