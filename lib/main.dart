import 'package:flutter/material.dart';
import 'sales_widget.dart';
import 'inventory_widget.dart';
import 'hr_widget.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'GAEA Backend',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentIndex = 0;
  //main pages
  final List<Widget> _children = [
    SalesWidget(Colors.amber),
    InventoryWidget(Colors.blueGrey),
    HRWidget(Colors.redAccent)
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
  
  @override
  Widget build(BuildContext context) {
    Widget currentWidget = _children[_currentIndex];

    return Scaffold(
      
      body: currentWidget,
      // floatingActionButton: FloatingActionButton(
      //   onPressed: null,
      //   tooltip: 'Increment',
      //   child: Icon(Icons.add),
      // ), // This trailing comma makes auto-formatting nicer for build methods.
      bottomNavigationBar: BottomNavigationBar(
       onTap: onTabTapped, // new
       currentIndex: _currentIndex, // new
       items: [
         new BottomNavigationBarItem(
           icon: Icon(Icons.attach_money),
           title: Text('Sales'),
         ),
         new BottomNavigationBarItem(
           icon: Icon(Icons.reorder),
           title: Text('Inventory'),
         ),
         new BottomNavigationBarItem(
           icon: Icon(Icons.person),
           title: Text('HR')
         )
       ],
     ),
    );
  }
}
